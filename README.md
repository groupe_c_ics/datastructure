# DataStructure

<b>Victor Minon </b><br>
<b>Sahan Kananke Acharige </b><br>
3ICS

<i>Project to manipulate the different data structures</i>
 ***
<br>

## Project

The goal of this project is to implement different functions allowing to manipulate stacks, queues, lists and heaps. <br>
<br>

## Functions 

Explicit list of classic functions created:
- initialize
- push
- pop
- peek
- duplicate
- insert
- remove
- replace
- clear

and other but all the code its correctly documented and you can open all the documentation with the following commands.

<br>

## Usage des commandes 
- make all : start the program<br>
- make documentation : display the documentation with doxygen<br>
- make clean : clean up directories containing source code

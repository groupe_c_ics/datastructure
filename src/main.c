/**
 * @file main.c
 * @author Victor Minon & Sahan Kananke Acharige
 * @version 1.5
 * @brief 
 * @date 2021-10-13
 * 
 * @copyright Copyright (c) 2021
 */

/*! \mainpage Accueil
 *
 *The goal of this project is to implement different functions to manipulate stacks, queues, lists and heaps.
 *The implementation will be based on the use of tables.

 * \subsection step1 Command :
 *make all: execute the program
 *\n make documentation: display this page
 *\n make clean: clean the directories containing the source code
 */


#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#include "../include/stack.h"
#include "../include/queue.h"
#include "../include/array_list.h"
#include "../include/heap.h"

#include <CUnit/CUnit.h> 
#include <CUnit/Basic.h>


// /**
//  * @brief tests the stack's functions
//  */
// void test_stack()
// {
//     Stack s;
//     init_stack(&s);
//     push_stack(&s, 1);
//     push_stack(&s, 1);
//     push_stack(&s, 3);
//     push_stack(&s, 9);
//     push_stack(&s, 6);
//     assert(s.data[4] == 6);
//     assert(pop_stack(&s) == 6);
//     assert(s.data[3]==9);
//     assert(is_stack_empty(&s) == false);
//     dup_stack(&s);
//     assert(peek_stack(&s) == 9);
//     clear_stack(&s);
// }

// /**
//  * @brief tests the queue's functions
//  */
// void test_queue()
// {
//     Queue q;
//     init_queue(&q);
//     enqueue(&q, 1);
//     enqueue(&q, 1);
//     enqueue(&q, 3);
//     enqueue(&q, 9);
//     enqueue(&q, 6);
//     dequeue(&q);
//     assert(front_queue(&q) == 1);
//     assert(q.data[0] == 1);
// }

// /**
//  * @brief tests the array_list's functions 
//  */
// void test_arraylist()
// {
//     Array_list l;
//     init_array_list(&l);
//     add_array_list(&l, 1);
//     add_array_list(&l, 1);
//     add_array_list(&l, 3);
//     add_array_list(&l, 9);
//     add_array_list(&l, 6);
//     assert(get_at(&l, l.index-1) == 6);
//     remove_at(&l, l.index-1);
//     assert(get_at(&l, l.index-1) == 9);
//     clear_array_list(&l);
// }

// /**
//  * @brief tests the heap's functions
//  */
// void test_heap()
// {
//     Heap h;
//     init_heap(&h);
//     push_heap(&h, 1);
//     push_heap(&h, 1);
//     push_heap(&h, 3);
//     push_heap(&h, 9);
//     push_heap(&h, 6);
//     assert(is_heap_empty(&h) == false);
//     assert(peek_heap(&h) == 1);
// }




/**
 * @brief tests the stack's functions with CUnit
 */
void test_stack_CU()
{
    Stack s;
    init_stack(&s);
    push_stack(&s, 1);
    push_stack(&s, 1);
    push_stack(&s, 3);
    push_stack(&s, 9);
    push_stack(&s, 6);
    CU_ASSERT_EQUAL(s.data[4],6);
    CU_ASSERT_EQUAL(pop_stack(&s),6);
    CU_ASSERT_EQUAL(s.data[3],9);
    CU_ASSERT_EQUAL(is_stack_empty(&s),false);
    dup_stack(&s);
    CU_ASSERT_EQUAL(peek_stack(&s),9);
    clear_stack(&s);
}

/**
 * @brief tests the queue's functions with CUnit
 */
void test_queue_CU()
{
    Queue q;
    init_queue(&q);
    enqueue(&q, 1);
    enqueue(&q, 1);
    enqueue(&q, 3);
    enqueue(&q, 9);
    enqueue(&q, 6);
    dequeue(&q);
    CU_ASSERT_EQUAL(front_queue(&q),1);
    CU_ASSERT_EQUAL(q.data[0],1);
}

/**
 * @brief tests the array_list's functions with CUnit
 */
void test_arraylist_CU()
{
    Array_list l;
    init_array_list(&l);
    add_array_list(&l, 1);
    add_array_list(&l, 1);
    add_array_list(&l, 3);
    add_array_list(&l, 9);
    add_array_list(&l, 6);
    CU_ASSERT_EQUAL(get_at(&l, l.index-1),6);
    remove_at(&l, l.index-1);
    CU_ASSERT_EQUAL(get_at(&l, l.index-1),9);
    clear_array_list(&l);
}

/**
 * @brief tests the heap's functions
 */
void test_heap_CU()
{
    Heap h;
    init_heap(&h);

    push_heap(&h, 1);
    push_heap(&h, 1);
    push_heap(&h, 3);
    push_heap(&h, 9);
    push_heap(&h, 6);
    CU_ASSERT_EQUAL(is_heap_empty(&h),false);
    CU_ASSERT_EQUAL(peek_heap(&h),1);
}


int init(void){

    return 0;
}

int clear_up(void){
     return 0;
}


/**
 * @brief main execute test function for all data structure 
 * @param argc : 
 * @param argv : 
 * @return int : code 
 */
int main(int argc, char** argv) {

     CU_initialize_registry();
     CU_Suite *suite = CU_add_suite("tests",init,clear_up);
     CU_add_test(suite, "test queue",test_queue_CU);
     CU_add_test(suite, "test heap",test_heap_CU);
     CU_add_test(suite, "test stack",test_stack_CU);
     CU_add_test(suite, "test array list",test_arraylist_CU);
     CU_basic_run_tests();

    // test_arraylist();
    // test_stack();
    // test_queue();
    // test_heap();
    
    return (EXIT_SUCCESS);
}


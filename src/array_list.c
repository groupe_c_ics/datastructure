/**
 * @file array_list.c
 * @author Victor Minon & Sahan Kananke Acharige
 * @version 1.0
 * @brief function for manipulate array list 
 * @date 2021-10-16
 * 
 * @copyright Copyright (c) 2021
 */

#include "../include/array_list.h"

/**
 * @brief initialize a array list
 * @param l : address of the array_list structure to initialized
 */
void init_array_list(Array_list *l){

    for(int i = 0; i < ARRAY_LIST_MAX_SIZE; i++)
    {
        l->data[i] = 0.0;
    }
    l->index = 0;
}

/**
 * @brief insert a value at a define position in a array
 * @param l : address of the array who will receive value 
 * @param position : value to be added
 * @param value : value to be added
 */
void insert_at(Array_list *l, int position, float value)
{
    if(l->index >= position)
    {
        for (int i = l->index; i > position; i--)
        {
            l->data[i] = l->data[i-1];
        }

        l->data[position] = value;
        l->index++;
        
    }
}

/**
 * @brief add a value at the end of a array
 * @param l : address of the array who will receive value 
 * @param value : value to be added
 */
void add_array_list(Array_list *l, float value)
{
    l->data[l->index] = value;
    l->index++;
}

/**
 * @brief return and remove a value of a stack 
 * @param l : address of the array to be modified
 * @param position : index of the value to be returned and removed
 * @return float : removed value
 */
float remove_at(Array_list *l, int position)
{

    if(l->index > position)
    {
        float ret = l->data[position];
        
        for (int i = position; i < l->index; i++)
        {
            l->data[i] = l->data[i+1];
        }
        l->index--;
        return ret;
    }
    
}

/**
 * @brief get a value in a array 
 * @param l : address of the array to be treated
 * @param position : index of the value to be returned
 * @return float : value at the position 
 */
float get_at(Array_list *l,int position)
{
    return l->data[position];
}

/**
 * @brief clear a array list
 * @param l : address of the array to be cleared
 */
void clear_array_list(Array_list *l)
{
    int max = ARRAY_LIST_MAX_SIZE;
    for(int i = 0; i < max; i++)
    {
        l->data[i] = 0.0;
    }
    l->index = 0;
}
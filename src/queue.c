/**
 * @file queue.c
 * @author Victor Minon & Sahan Kananke Acharige
 * @version 1.0
 * @brief function for manipulate queue
 * @date 2021-10-13
 * 
 * @copyright Copyright (c) 2021
 */

#include "../include/queue.h"

/**
 * @brief initialize a new queue
 * @param q address of the queue to initialized 
 */
void init_queue(Queue *q){
    
    for(int i = 0; i < QUEUE_MAX_SIZE; i++)
    {
        q->data[i] = 0.0;
    }
    q->index = 0;
}

/**
 * @brief add a value to a queue
 * @param q address of the queue to append a new value
 * @param value float value to be added to the queue
 */
void enqueue(Queue *q, float value){
    q->data[q->index] = value;
    q->index++;
}

/**
 * @brief return and remove the first value of a queue 
 * @param q address of the queue to be modified
 * @return float : last value of the queue 
 */
float dequeue(Queue *q)
{
    float ret = q->data[0];
    if(q->index != 0){
    for(int i = 0; i < q->index; i++)
    {
        q->data[i] = q->data[i+1];
    }
    }
    return ret;
}

/**
 * @brief test if a queue is empty or not
 * @param q : address of the queue to be analyse
 * @return true : queue is empty
 * @return false : queue is not empty
 */
bool is_queue_empty(Queue *q)
{
    if(q->index == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * @brief return the first value of a queue
 * @param q : address of the queue to be treated
 * @return float : last value of the queue
 */
float front_queue(Queue *q)
{
    return q->data[0];
}


/**
 * @brief clear a queue 
 * @param q : address of the queue to be cleared
 */
void clear_queue(Queue *q)
{
    int max = QUEUE_MAX_SIZE;
    for(int i = 0; i < max; i++)
    {
        q->data[i] = 0.0;
    }
    q->index = 0;
}
/**
 * @file heap.c
 * @author Victor Minon & Sahan Kananke Acharige
 * @version 1.0
 * @brief function for manipulate heap 
 * @date 2021-10-13
 * 
 * @copyright Copyright (c) 2021
 */

#include "../include/heap.h"

/**
 * @brief initialize a new heap
 * @param h : address of the heap to initialized
 */
void init_heap(Heap *h)
{
    for (int i = 0; i < HEAP_MAX_SIZE; i++)
    {
        h->data[i] = 0.0;
    }
    h->index = 0;
}

/**
 * @brief test if a heap is empty or not
 * @param h : address of the heap to be analyse 
 * @return true : heap is empty
 * @return false : heap is not empty
 */
bool is_heap_empty(Heap *h)
{
    if (h->index == 1 && h->data[0] == 0.0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * @brief construct a tree in a heap, from a table
 * @param h : address of the heap in which the tree is going to be built
 * @param array : heap data table
 * @param array_size : number of value in the table  
 */
void heapify(Heap *h, float array[], size_t array_size)//create Heap from array
{
    int noeud = array_size/2;
    int j = 2*noeud;
    int taille = array_size;
    while (j <= taille)
    {
        if ((j < taille) && (array[j] < array[j+1]))
        {
            j++;
        }
        else if (array[noeud] < array[j])
        {
            float temp = array[noeud];
            array[noeud] = array[j];
            array[j] = temp;
            noeud = j;
            j = 2*noeud;
        }
        else {
            j = taille+1;
        }
    }
    for (int i = 0; i <taille; i++){
        h->data[i] = array[i];
    }
}

/**
 * @brief return and remove the first value of a heap, 
 * and rebuild the tree of the heap 
 * @param h : address of the heap to be modified 
 * @return float : first value of the heap
 */
float pop_heap(Heap *h)
{
    float ret = h->data[0];
    h->data[0] = 0.0;
    heapify(h, h->data,h->index);
    h->index--;
    return ret;
}

/**
 * @brief add a value to a heap,
 * and rebuild the tree of the heap 
 * @param h : address of the heap to be modified 
 * @param value : value to be added to the heap
 */
void push_heap(Heap *h, float value)
{
    h->data[h->index] = value;
    heapify(h, h->data,h->index);
    h->index++;
}

/**
 * @brief return the first value of a heap and replace it by a new value,
 * and rebuild the tree of the heap 
 * @param h : address of the heap to be modified
 * @param value : replaced value
 * @return float : old first value
 */
float replace_heap(Heap *h, float value)// pop root and push a new key.
{
    pop_heap(h);
    push_heap(h, value);
}

/**
 * @brief return the first value of a heap
 * @param h : address of the heap to be treated 
 * @return float : first value of the heap
 */
float peek_heap(Heap *h)// return root value but dont remove it
{
    return h->data[0];
}

/**
 * @brief copy a heap in an other heap 
 * @param dest : address of the destination heap
 * @param src : address of the source heap 
 */
void merge(Heap *dest, Heap *src)
{
    int max = HEAP_MAX_SIZE;
    if (dest->index + src->index < max)
    {
        int j = 0;
        for (int i = dest->index; i < dest->index+src->index; i++)
        {
            dest->data[i] = src->data[j];
            j++;
        }
    }
}
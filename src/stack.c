/**
 * @file stack.c
 * @author Victor Minon & Sahan Kananke Acharige
 * @version 1.0
 * @brief function for manipulate stack 
 * @date 2021-10-13
 * 
 * @copyright Copyright (c) 2021
 */

#include "../include/stack.h"

/**
 * @brief initialize a new stack
 * @param s : address of the stack to initialized 
 */

void init_stack(Stack *s){
    int max = STACK_MAX_SIZE;
    for (int i = max; i!=0; i--)
    {
        (*s).data[i]=0.0;
    }
    (*s).index=0;
}

/**
 * @brief add a value to a stack
 * @param s : address of the stack to append a new value
 * @param value : value to be added to the stack 
 */
void push_stack(Stack *s, float value) {
    if ((*s).index < STACK_MAX_SIZE)
    {
        (*s).data[(*s).index] = value;
        (*s).index++;
    }  
}

/**
 * @brief return and remove the last value of a stack 
 * @param s address of the stack to be modified
 * @return float : last value of the stack
 */
float pop_stack(Stack *s) {
    float value = (*s).data[(*s).index-1];
    (*s).index--;
    return value;
}


/**
 * @brief test if a stack is empty or not
 * @param s address of the stack to be analyse
 * @return true : stack is empty 
 * @return false : stack is not empty
 */
bool is_stack_empty(Stack *s){
    bool ret = false;
    if ((*s).index==0)
    {
        ret= true;
    }
    return ret;
}


/**
 * @brief return the last value of a stack 
 * @param s address of the stack to be treated
 * @return float : last value of the stack 
 */
float peek_stack(Stack *s){
    float value = (*s).data[(*s).index-1];
    return value;
}


/**
 * @brief duplicate the last value of a stack 
 * @param s address of the stack to be modified
 */
void dup_stack(Stack *s){
    push_stack(s,peek_stack(s));
}


/**
 * @brief swap the last value with the before-last value of a stack 
 * @param s address of stack to be modified
 */
void swap_stack(Stack *s){
    if ((*s).index>1)
    {
        float last, beforelast;
        last = (*s).data[(*s).index-1];
        beforelast = (*s).data[(*s).index-2];
        pop_stack(s);
        pop_stack(s);
        push_stack(s,beforelast);
        push_stack(s,last);
    }
}   
    


/**
 * @brief clear a stack
 * @param s address of stack to be cleared
 */
void clear_stack(Stack *s){
    init_stack(s);
}


